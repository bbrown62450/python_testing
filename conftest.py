import pytest
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities


@pytest.fixture(name="driver", scope="class")
def driver(request):
    driver = webdriver.Remote("http://127.0.0.1:4444/wd/hub", DesiredCapabilities.CHROME)
    driver.implicitly_wait(5)
    driver.maximize_window()

    yield driver
    driver.close()
